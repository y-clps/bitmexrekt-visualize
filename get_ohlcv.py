# coding: utf-8

import requests
import json
import csv

def get_chart(market,minutes):
    seconds = int(minutes*60)

    while(1):
        try:
            if market=="bf":
                res = json.loads(requests.get("https://api.cryptowat.ch/markets/bitflyer/btcjpy/ohlc?periods=%s" %seconds).text)["result"]
            if market=="bffx":
                res = json.loads(requests.get("https://api.cryptowat.ch/markets/bitflyer/btcfxjpy/ohlc?periods=%s" %seconds).text)["result"]
            if market=="bffuture":
                res = json.loads(requests.get("https://api.cryptowat.ch/markets/bitflyer/btcjpy-quarterly-futures/ohlc?periods=%s" %seconds).text)["result"]
            if market=="mex":
                res = json.loads(requests.get("https://api.cryptowat.ch/markets/bitmex/btcusd-perpetual-futures/ohlc?periods=%s" %seconds).text)["result"]
        except:
            pass
        else:
            break
        
    time = [float(res["%s" %seconds][i][0]) for i in range(0,500)]
    open = [float(res["%s" %seconds][i][1]) for i in range(0,500)]
    high = [float(res["%s" %seconds][i][2]) for i in range(0,500)]
    low = [float(res["%s" %seconds][i][3]) for i in range(0,500)]
    close = [float(res["%s" %seconds][i][4]) for i in range(0,500)]

    return time,open,high,low,close
